import Session from "@/types/Session";
import { sessionStore } from "@/store/session";

export default defineNuxtRouteMiddleware((to) => {

    const session:Session = sessionStore().getSession;
    if (session.sessionID.trim().length == 0) {
        navigateTo("/login");
    }
    else {
        navigateTo(to);
    }
});