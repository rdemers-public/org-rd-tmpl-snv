import axios from 'axios'

export default defineNuxtPlugin(nuxtApp => {
    return {
        provide: {
            axios: () => {
                return axios.create(
                {
                    /* configuration here */
                })
            }
        }
    }
})

/*
import { defineNuxtPlugin } from '#app'
import axios from 'axios'

export default defineNuxtPlugin(() => {
  return {
    provide: {
      axios: () => {
        return axios.create({configuration here })
    }
}
}
})

<script setup lang="ts">
import { useNuxtApp } from '#app'

let { $axios } = useNuxtApp()
const axios = $axios()
axios.get('/req/path/123').then((response) => {
  do things with response and response.data 
})
</script>
*/