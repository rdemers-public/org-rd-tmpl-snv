import { defineStore } from "pinia";
import Session from "@/types/Session";

export const sessionStore = defineStore({
    id: "session",

    state: () => ({
        currentSession: {
            sessionID: "",
            token: "",
            user: "",
            password: "",
            roles: [""],
        } as Session,
    }),

    getters: {
        getSession():Session {
            return this.currentSession;
        }
    },

    actions: {
        setSession(session: Session) {
            this.currentSession = session;
        },

        clearSession() {
            this.currentSession = {
                sessionID: "",
                token: "",
                user: "",
                password: "",
                roles: [""],
            };
        }
    }
})
