// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({

    css: [  "vuetify/lib/styles/main.sass", 
            "@mdi/font/css/materialdesignicons.min.css"],

    experimental: {
        payloadExtraction: false,
    },

    build: {
        transpile: ["vuetify"],
    },

    modules: [
        [
            "@pinia/nuxt", {
                autoImports: [
                    "defineStore",
                    [ "defineStore", "definePiniaStore", "acceptHMRUpdate" ],
                ],
            },
        ],
    ],

    app: {
        //baseURL: "/app",
        //head: {
        //    title: "App",
        //   link: [{ 
        //           rel: "icon", type: "image/x-icon", href: "/app/favicon.ico" 
        //       }
        //    ],
        // },
    },

    vite: {
        define: {
            "process.env.DEBUG": false,
        },
    },
});
