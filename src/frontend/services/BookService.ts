/**
  * ----------------------------------------------------------------------------
  * Name        : BookService.
  * Description : I/O for books.
  * ----------------------------------------------------------------------------
  */
import http from "@/services/http-common";

class BookService {

    /**
      * ------------------------------------------------------------------------
      * @remarks
      * This method return all books.
      *
      * @returns List of books.
      * ------------------------------------------------------------------------
      */     
    getAll(): Promise<any> {
        return http.get("/books");
    }

    /**
      * ------------------------------------------------------------------------
      * @remarks
      * This method create a book.
      *
      * @param id - Book.id
      *
      * @returns Book.
      * ------------------------------------------------------------------------
      */
    get(id: any): Promise<any> {
        return http.get("/books/${id}");
    }

    /**
      * ------------------------------------------------------------------------
      * @remarks
      * This method create a book.
      *
      * @param data - Book.
      *
      * @returns None.
      * ------------------------------------------------------------------------
      */  
    create(data: any): Promise<any> {
        return http.post("/books", data);
    }

    update(id: any, data: any): Promise<any> {
        return http.put("/books/${id}", data);
    }

    delete(id: any): Promise<any> {
        return http.delete("/books/${id}");
    }

    deleteAll(): Promise<any> {
        return http.delete("/books");
    }

    findByTitle(title: string): Promise<any> {
        return http.get("/books?title=${title}");
    }
}

export default new BookService();