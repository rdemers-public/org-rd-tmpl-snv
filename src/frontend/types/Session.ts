export default interface Session {
    sessionID: string;
    token: string;
    user: string;
    password: string;
    roles: string[];
}
