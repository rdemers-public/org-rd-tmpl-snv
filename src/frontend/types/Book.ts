export default interface Book {
    id: null;
    title: string;
    description: string;
}
